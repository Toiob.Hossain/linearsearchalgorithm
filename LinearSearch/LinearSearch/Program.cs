using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            //Linear Search Algorithm
            int[] NumberList = new int[] { 23, 45, 22, 67, 44, 11, 32, 78, 95, 34, 20 }; //array declare
            int item = 220,count=0; //item and count variable
            bool FindItem = false; //
            for(int i=0;i<NumberList.Length;i++) // using for loop check every value
            {
                if(item==NumberList[i]) //using if loop find out search value
                {
                    FindItem = true;
                    Console.WriteLine("Item index is = " + i);
                    break;
                }
                count++;
            }
            if(!FindItem) // not find value
                {
                Console.WriteLine("Item not Found!");

            }
            Console.WriteLine("Total Count Number = " + count);
            Console.ReadKey();

            
        }
    }
}
